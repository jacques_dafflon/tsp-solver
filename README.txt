TSPSolver
Author: Jacques Dafflon

## Running the program:

1. Compile all the classes.

2. From the terminal go to the folder containing the main class.
   The main class is located at: ./src/ch/usi/inf/ikm/tsp.

3. Run the Program from ./src as follow:
   $ java java ch.usi.inf.ikm.tsp.Main path_to_the_tsp_file [ seed ]

   Where path to the tsp file is the path to the file with the .tsp extension which contains the tsp problem.

   And seed is a an optional parameter to set the seed of the random generator used by the algorithm.

   The default value is the current system time in nano-seconds

4. The program will generate a folder called out in the folder where the tsp file is located.
   The out folder contains a .tour file containing the tour and a .dot file containing the tour as a graphviz formatted graph.