package ch.usi.inf.ikm.tsp;

public class DistanceMatrix {
    
    private final int size;
    private final int matrix[];
    
    public DistanceMatrix(final int capacity) {
        this.size = capacity;
        this.matrix = new int[this.size * this.size + 1];
    }
    
    public int get(final int a, final int b) {
        if (a < b) {
            return this.matrix[a * this.size + b];
        } else if (b < a) {
            return this.matrix[b * this.size + a];
        } else {
            return 0;
        }
        
    }
    
    public void put(final int a, final int b, final int distance) {
        if (a < b) {
            this.matrix[a * this.size + b] = distance;
        } else if (b < a) {
            this.matrix[b * this.size + a] = distance;
        }
    }
    
    public static int computeDistance(final double aX, final double aY, final double bX, final double bY) {
        return Math.round((float) Math.sqrt(Math.pow(aX - bX, 2) + Math.pow(aY - bY, 2)));
    }
}
