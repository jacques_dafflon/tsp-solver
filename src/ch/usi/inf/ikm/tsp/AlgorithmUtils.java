package ch.usi.inf.ikm.tsp;

import java.util.Random;

public class AlgorithmUtils {
    
    public static long SEED = System.nanoTime();
    public static final Random RANDOM = new Random(AlgorithmUtils.SEED);
    
    public static int[] nearestNeighbour(final int[] nodes, final double[] nodesX, final double[] nodesY,
        final int size, final DistanceMatrix matrix) {
        final int[] tour = new int[size];
        final int[] toVisit = new int[size];
        System.arraycopy(nodes, 0, toVisit, 0, nodes.length);
        
        int currentIndex = 0;
        int nextIndex = -1;
        int min = -1;
        tour[0] = nodes[currentIndex];
        for (int i = 1; i < toVisit.length; i++) {
            for (int j = 1; j < toVisit.length; j++) {
                if (toVisit[j] != -1 && (nextIndex == -1 || matrix.get(nodes[currentIndex], nodes[j]) < min)) {
                    min = matrix.get(nodes[currentIndex], nodes[j]);
                    nextIndex = j;
                }
            }
            tour[i] = nodes[nextIndex];
            toVisit[nextIndex] = -1;
            currentIndex = nextIndex;
            nextIndex = -1;
        }
        
        return tour;
    }
    
    public static int[] twoOpt(final int[] nodes, final double[] nodesX, final double[] nodesY,
        final int size, final DistanceMatrix matrix, final int[] tour) {
        
        int[] bestTour = tour.clone();
        int bestDistance = NodeUtils.computeTourDistance(bestTour, matrix);
        int[] newTour = new int[size];
        int newDistance = -1;
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                newTour = AlgorithmUtils.twoOptSwap(bestTour, size, i, j);
                newDistance = NodeUtils.computeTourDistance(newTour, matrix);
                if (newDistance < bestDistance) {
                    bestTour = newTour.clone();
                    bestDistance = newDistance;
                }
            }
        }
        return bestTour;
        
    }
    
    private static int[] twoOptSwap(final int[] tour, final int size, final int i, final int k) {
        final int[] newTour = new int[size];
        int tmp = -1;
        if (i > 0) {
            System.arraycopy(tour, 0, newTour, 0, i);
        }
        final int middle = (k - i + 1) / 2;
        for (int j = 0; j <= middle; j++) {
            tmp = tour[k - j];
            newTour[k - j] = tour[i + j];
            newTour[i + j] = tmp;
        }
        System.arraycopy(tour, k + 1, newTour, k + 1, size - k - 1);
        return newTour;
    }
    
    public static int[] simulatedAnnealing(final int[] nodes, final double[] nodesX, final double[] nodesY,
        final int size, final DistanceMatrix matrix, final int[] tour) {
        final long start = System.nanoTime();
        
        int[] currentTour = tour.clone();
        int currentEnergy = NodeUtils.computeTourDistance(currentTour, matrix);
        
        int[] bestTour = tour.clone();
        int bestEnergy = currentEnergy;
        
        int[] nextTour = currentTour.clone();
        int nextEnergy = currentEnergy;
        
        double temp = 100;
        final double coolingRate = size > 700 ? 0.94 : 0.97;
        final double minTemp = 1;
        final int iteration = size > 700 ? 1 : 20 * size;
        
        int delta = -1;
        int pos1 = 1;
        int pos2 = 1;
        
        System.out.println("Simulated Annealing, initial distance " + currentEnergy);
        int tmp = -1;
        boolean update = false;
        while (temp > minTemp) {
            for (int i = iteration; i > 0; --i) {
                if (update) {
                    nextTour = currentTour.clone();
                }
                pos1 = AlgorithmUtils.RANDOM.nextInt(size);
                pos2 = AlgorithmUtils.RANDOM.nextInt(size);
                while (pos2 == pos1) {
                    pos2 = AlgorithmUtils.RANDOM.nextInt(size);
                }
                tmp = nextTour[pos1];
                nextTour[pos1] = nextTour[pos2];
                nextTour[pos2] = tmp;
                nextEnergy = NodeUtils.computeTourDistance(nextTour, matrix);
                
                if (nextEnergy * 0.7 < currentEnergy) {
                    nextTour = AlgorithmUtils.twoOpt(nodes, nodesX, nodesY, size, matrix, nextTour);
                    nextEnergy = NodeUtils.computeTourDistance(nextTour, matrix);
                }
                
                delta = nextEnergy - currentEnergy;
                update = true;
                
                if (delta < 0) {
                    currentTour = nextTour.clone();
                    currentEnergy = nextEnergy;
                    
                    update = false;
                    if (nextEnergy < bestEnergy) {
                        System.out.println("Better path: " + bestEnergy + " -> " + nextEnergy);
                        bestTour = nextTour.clone();
                        bestEnergy = nextEnergy;
                    }
                } else if (AlgorithmUtils.RANDOM.nextDouble() < Math.exp(-delta / temp)) {
                    currentTour = nextTour.clone();
                    currentEnergy = nextEnergy;
                    update = false;
                }
                if ((System.nanoTime() - start) / 1000000000.0 > 150) {
                    break;
                }
            }
            
            if ((System.nanoTime() - start) / 1000000000.0 > 150) {
                break;
            }
            temp *= coolingRate;
            
        }
        System.out.println("Seed: " + AlgorithmUtils.SEED);
        return bestTour;
    }
}
