package ch.usi.inf.ikm.tsp;

public class Main {
    public static void main(final String[] args) {
        if (args.length < 1) {
            System.err.println("Usage: java Main.class tsp_file [random_seed]");
            System.exit(-1);
        }
        if (args.length == 2) {
            AlgorithmUtils.SEED = Long.parseLong(args[1]);
            AlgorithmUtils.RANDOM.setSeed(AlgorithmUtils.SEED);
        }
        final long startTime = System.nanoTime();
        final TSPBuilder builder = new TSPBuilder(args[0]);
        final int[] nodes = builder.getNodes();
        final double[] nodesX = builder.getNodesX();
        final double[] nodesY = builder.getNodesY();
        final int size = builder.getDimension();
        final DistanceMatrix distanceMatrix = NodeUtils.computeDistanceMatrix(size, nodes, nodesX, nodesY);
        int[] tour = AlgorithmUtils.nearestNeighbour(nodes, nodesX, nodesY, size, distanceMatrix);
        tour = AlgorithmUtils.twoOpt(nodes, nodesX, nodesY, size, distanceMatrix, tour);
        tour = AlgorithmUtils.simulatedAnnealing(nodes, nodesX, nodesY, size, distanceMatrix, tour);
        final int cost = NodeUtils.computeTourDistance(tour, distanceMatrix);
        System.out.println("Final Distance: " + cost);
        final long endTime = System.nanoTime();
        final long duration = endTime - startTime;
        final double seconds = duration / 1000000000.0;
        System.out.println("Duration: " + seconds);
        System.out.println("Path: " + NodeUtils.tourOnSingleLine(tour));
        NodeUtils.generateTourFile(builder, tour, cost, duration);
        NodeUtils.generateDotFile(builder, tour, cost, nodes, nodesX, nodesY, duration);
        System.exit(0);
    }
}
